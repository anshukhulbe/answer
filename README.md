# Answer

Answer to the Question 1
bugs in home page
* Language toggling between English and Germany doesn't change the language of the page. The page remains in German even after clicking on English
* The website can be opened over HTTP, HTTP requests need to be blocked or redirected to https
* There is inconsistency in the length of the booking reference number. The description says it to be the ten characters alphanumeric string but the error says it to be of t lest six characters.  
* White spaces should not be accepted in booking reference number (for example six space are being considered as a valid booking reference number)
* Nonalphanumneric characters are being accepted as part of booking reference  number for example (1234$% and 12345#)
* Nonalphanumeric characters are being accepted as part of name such as (@#Anshu$)


Bugs in Checking details page
* No way to go back to the home page (make, logo clickable or provide navigation options)
* Language doesn't toggle between EN and DE
* User is able to specify future Date of birth (20/08/2020)
* Button to brass passport picture is not as per best UI experience (dimensions, placement, alignment, colors)
* The first name is auto-populating as MAX
* Drop Down menu of Country and nationality are a different size and positioning and overlapping 
* white spaces are being accepted in all the fields such as Passport number, zip code, etc  such as 6 spaces
* Back button behavior seems to be undefined and inconsistent (may cause data corruption)
* highlight fields with invalid inputs more clearly with the use of color or another mark'
* Do image processing of passport picture and fetch user details to match it with the values user has entered 
* User can upload any image in place of a valid passport (there should be some validation on the server side to make sure that uploaded image is the image of at least a passport
* Inform user explicitly that she needs to upload front bad back both the pages of her passport
* If the keyboard is being used to traverse the drop-down menu of  county and nationality then two values are being selectedTest Scenarios

Bugs in submit page
*  request submitted message is shown.Error is not shown for wrong request although  Post request give error message


**Answer 2**
Input validations
* Min and MAX length of various fields 
* Empty fields (don't specify any input)
* valid characters (alphanumeric)
* Input with special characters such as !@#$
* handle cache - the user should get an error message of he is not connected to the internet 

Usability
* Page layout
* Ease of navigation
* Appropriate error message with actionable information 
* Handle network fluctuations
* Mobile phone specific features (launch camera to take the photo of passport) 

Accessibility testing
* Test as per policy 508 and country-specific compliances


Testing on various network speed 
* 4G
* Wi-FI
* 3G
* 2G/EDGE

Localization 
* input should be tested for DE and EN both
* Test on machines.devices running OS in DE language


Mobile adaptability
* Make use of tools such as  Google’s mobile-friendly test.
* Test on various screen sizes having different resolutions
* Test it in portrait and landscape mode both
* Test it in Auto Rotate screen 



Non-functional test cases

* Performance 
* Page load timing
* big input
* concurrent requests
* image upload time
* Caching/CDN

2. Cross-browser and cross-OS testing (use tools like browser stack)
* Desktop (Windows, MAC, Linux)
    * Chrome
    * IE
    * FF
    * Safari
Mobile
* Android 
* Up to Current -3 android version 
* chrome
* FF
* IOS
* Up to current-1 IOS version 
* Safari 
* Chrome
* Others 
* if required 

3. Security
* don't run the website over HTTP
* Handle DOS attacks
* CSS
* Use JWS tokens
* handle sessions securely
* make use of Post request where ever possible
* User data security 
* Data Retention policy 
* access control
* secure purging 

4 Backend testing
* Test different web services 
* Response time
* Response code
* Error codes
* time outs
* Authentication/Authorisation
* Database 
* SQL queries (join etc)
* the optimal number reads and writes 
* Replication


Automation - 
End to End testing using a tool like selenium 
I 'll take a data-driven approach where I ll specify my inputs in a file which ll be read by automation framework which will later interact with web pages and look for actual output. The expected output for each info will also be specified in the same file. In the last automation framework will compare the expected output with actual output. 
I 'll also integrate my framework with bug tracking stem and test case management for auto-creation of bugs and auto-updating of test cases results in ll later integrate it with CI/CD like Jenkins where test cases might be triggered for every check-in, 

Backend automtion
Here I ll wrte a framwerk, proabaly i ll use rest assured to create a request and fire it. I 'll also use TestNG or a similar framework to generates test reports. This should also be the art of CI/CD. This framework later can be enhanced to perform load and performance testing.


**Answer 3**

Summary: DOB field is accepting future dates.
priotity:p1
severity:S1
reproducibility:Always
regression:Not known
Depends on:Unknown
Related to:Unknown
Platform details
    Browser:Chrome
    Device:MacBook Pro
    OS:
    OS Version:
    Browser Version:
    Client:Desktp/Mobile
    language:English and DE
Description:
        Steps:
        1.go to https://limehome-qa-task.herokuapp.com
        2.Enter Last Name as "test" and booking reference as "123456"
        3.click on submit button
        4.Enter all the input field and DOB as "11/7/2039"
        5.click on submit

Actual output: User is taken to https://limehome-qa-task.herokuapp.com/complete page and shown message as completed
Expected Output : User should not be allowed to DOB > current date

logs for debug

API call 
zone.js:3243 POST http://localhost:44343/api/LimehomeRegistrationForms net::ERR_CONNECTION_REFUSED
scheduleTask @ zone.js:3243
push../node_modules/zone.js/dist/zone.js.ZoneDelegate.scheduleTask @ zone
........
---------
complete.component.ts:36 Error: HttpErrorResponse {headers: HttpHeaders, status: 0, statusText: "Unknown Error", url: "http://localhost:44343/api/LimehomeRegistrationForms", ok: false, …}

not able to attach image


